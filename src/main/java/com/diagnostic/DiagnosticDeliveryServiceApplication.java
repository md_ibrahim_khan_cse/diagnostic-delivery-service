package com.diagnostic;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;



@SpringBootApplication
@EnableJpaAuditing()
public class DiagnosticDeliveryServiceApplication {

//	@Bean
//	public AuditorAware<String> auditorAware() {
//		return new AuditorAwareImpl(); auditorAwareRef = "auditorAware"
//	}
	

	public static void main(String[] args) {
		
		SpringApplication.run(DiagnosticDeliveryServiceApplication.class, args);
	}

}
