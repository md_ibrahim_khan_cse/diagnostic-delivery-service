package com.diagnostic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.diagnostic.model.Billing;
import com.diagnostic.service.BillingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin("*")
public class BillingController {

	
	@Autowired
	private BillingService billingService;
	
	
	// http://localhost:8080/billing/1
	@PostMapping("/billing/{patientId}")
	public ResponseEntity<?> fetchBill(@PathVariable(value = "patientId") int patientId,
			@RequestBody Billing billing) throws JsonMappingException, JsonProcessingException {
		
		
		System.out.println("billing " + billing.toString());
		Billing requestBilling = billingService.createBill(patientId, billing);

		if (requestBilling != null) {
			System.out.println("seccessfully.... " + requestBilling.toString());
			return ResponseEntity.ok().body("seccessfully created");
			//return "seccessfully";
		} else {
			return ResponseEntity.badRequest().body("Fail to create");
			//return "Fail";
		}
		
	}
	
	
	// http://localhost:8080/billing/1
	@GetMapping("/billing/{investigationRequestId}")
	public ResponseEntity<?> fetchInvestigationRequestByPrividerId(
			@PathVariable(value = "investigationRequestId") int investigationRequestId) {

		System.out.println("categoryId id " + investigationRequestId);

		Billing requestBilling = billingService.fetchBillingbyinvestigationRequestId(investigationRequestId);

		return ResponseEntity.ok().body(requestBilling);
	}
	
	
}


//{
//	"paymentValue":200,
//	"paymentType":"Bkash"
//}
