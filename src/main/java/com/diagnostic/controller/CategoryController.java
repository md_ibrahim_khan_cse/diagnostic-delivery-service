package com.diagnostic.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.diagnostic.model.Category;
import com.diagnostic.service.CategoryService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@CrossOrigin("*")
@RestController
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	// http://localhost:8080/category/save
	@PostMapping("/category/save")
	ResponseEntity<?> CreateCategory(@RequestBody Category category) {

		if (categoryService.CreateCategory(category) != 0) {
			return ResponseEntity.ok().body("successfully created");
		}

		return ResponseEntity.badRequest().body("Fail to create");
	}

	// http://localhost:8080/category/3
	@PutMapping("/category/{categoryId}")
	ResponseEntity<?> updateBook(@PathVariable(value = "categoryId") int categoryId, @RequestBody Category category)
			throws JsonParseException, JsonMappingException, IOException {

		System.out.println("category " + category.toString());

		Category resultCategory = categoryService.updateCategory(categoryId, category);

		if (resultCategory != null) {
			return ResponseEntity.ok().body("successfully updated");
		} else {
			return ResponseEntity.badRequest().body("Fail to update");
		}
	}
	
	
	// http://localhost:8080/category
    @GetMapping("/category")
    public ResponseEntity<List<Category>> fetchCategory() {
    	
    	List<Category> CategoryList = categoryService.fetchAllCategory();
    	
    	return ResponseEntity.ok().body(CategoryList);
    }

}

//{
//	"name":"name",
//	"description":"description"	
//}
