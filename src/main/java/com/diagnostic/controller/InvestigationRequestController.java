package com.diagnostic.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.function.ServerRequest.Headers;

import com.diagnostic.model.AdminInvestigationPost;
import com.diagnostic.model.BillAndInvestigationRequestApi;
import com.diagnostic.model.Billing;
import com.diagnostic.model.Investigation;
import com.diagnostic.model.InvestigationRequest;
import com.diagnostic.model.Patient;
import com.diagnostic.model.UserInvestigationReport;
import com.diagnostic.repository.BillingRepository;
import com.diagnostic.repository.InvestigationRepository;
import com.diagnostic.repository.InvestigationRequestRepository;
import com.diagnostic.repository.PatientRepository;
import com.diagnostic.service.BillingService;
import com.diagnostic.service.InvestigationRequestService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin("*")
@RestController
public class InvestigationRequestController {

	@Autowired
	private InvestigationRequestService investigationRequestService;

	@Autowired
	private InvestigationRequestRepository investigationRequestRepository;

	@Autowired
	private BillingService billingService;

	@Autowired
	private BillingRepository billingRepository;

	@Autowired
	private InvestigationRepository investigationRepository;
	
	@Autowired
	private PatientRepository patientRepository;
	
	
	
//	@Bean
//	public WebMvcConfigurer configure() {
//		return new WebMvcConfigurer() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//		registry.addMapping("/*").allowedOrigins("http://localhost:9090");
//			}
//			
//		};
//	}

	// http://localhost:8080/categoryinvestigation/1
	@PostMapping("/investigationrequest/{providerId}")
	ResponseEntity<?> createInvestigation(@PathVariable(value = "providerId") int providerId,
			@RequestParam("item") String selectedItemString, @RequestParam("patient") String patientObject)
			throws JsonParseException, JsonMappingException, IOException {

		Patient patient = new ObjectMapper().readValue(patientObject, Patient.class);

		String[] selectedItem = selectedItemString.split(",");

		List<InvestigationRequest> resultInvestigationRequest = investigationRequestService
				.createInvestigationRequest(providerId, selectedItem, patient);

		if (resultInvestigationRequest != null) {
			return ResponseEntity.ok().body("successfully created");
		} else {
			return ResponseEntity.badRequest().body("Fail to create");
		}

	}

	// http://localhost:8080/investigationrequestadmin/1
	@PostMapping("/investigationrequestadmin/{providerId}")
	ResponseEntity<?> createInvestigationRequestByAdmin(@PathVariable(value = "providerId") int providerId,
			@RequestParam("object") String Object, @RequestParam(required = false) MultipartFile reportImage)
			throws JsonParseException, JsonMappingException, IOException {

		AdminInvestigationPost object = new ObjectMapper().readValue(Object, AdminInvestigationPost.class);

		if (reportImage != null) {
			object.setImageFileName(reportImage.getOriginalFilename());
			object.setReportImage(reportImage.getBytes());
		}

		InvestigationRequest resultInvestigationRequest = investigationRequestService
				.createInvestigationRequestByAdmin(providerId, object);

		if (resultInvestigationRequest != null) {
			return ResponseEntity.ok().body(resultInvestigationRequest.getId());
		} else {
			return ResponseEntity.badRequest().body("Fail to create");
		}

//		return ResponseEntity.badRequest().body("Fail to create");

//		return ResponseEntity.ok().body("seccessfully created");
//		{"patientName":"name","mobile":"mobile","reportText":"reportText","investigationName":"Blood glucose test","referenceId":1}
//      formdata {"patientName":"Md Ibrahim Khan","mobile":"01521453788","investigationName":"Transesophageal echocardiography (TEE):","reportText":"good","referenceId":"2"}
	}

	// http://localhost:8080/sendmail/1/159
	@PostMapping("/sendmail/{investigationRequestId}/{providerId}")
	ResponseEntity<?> sendMail(@PathVariable(value = "investigationRequestId") int investigationRequestId,
			@PathVariable(value = "providerId") int providerId, @RequestBody String gmail)
			throws JsonParseException, JsonMappingException, IOException, MailException, MessagingException {

		int result = investigationRequestService.sendMailToPatient(gmail, providerId, investigationRequestId);

		if (result != 0) {
			return ResponseEntity.ok().body("successfully sent");
		} else {
			return ResponseEntity.badRequest().body("Fail to sent");
		}
//		ebrahimkhanobak@gmail.com

	}

	
	// http://localhost:8080/investigationrequest/1
	@GetMapping("/investigationrequest/{providerId}")
	public ResponseEntity<List<BillAndInvestigationRequestApi>> fetchInvestigation(
			@PathVariable(value = "providerId") int providerId) {

		List<BillAndInvestigationRequestApi> allInfo = new ArrayList<>();

		List<InvestigationRequest> investigationRequestList = investigationRequestService
				.fetchInvestigationRequestByProviderId(providerId);
		List<Billing> billingList = billingService.fetchAllBill();

		int max = Math.max(investigationRequestList.size(), billingList.size());

		for (int i = 0; i < max; i++) {

			BillAndInvestigationRequestApi object = new BillAndInvestigationRequestApi();

			if (billingList.size() > i) {
				object.setBill(billingList.get(i));
			} else {
				object.setBill(null);
			}

			if (investigationRequestList.size() > i) {
				object.setInvestigationRequest(investigationRequestList.get(i));
			} else {
				object.setInvestigationRequest(null);
			}

			allInfo.add(object);

		}

		return ResponseEntity.ok().body(allInfo);
	}

	// http://localhost:8080/investigationuserreport/1/01521453788
	@GetMapping("/investigationuserreport/{providerId}/{mobile}")
	public ResponseEntity<List<UserInvestigationReport>> fetchUserInvestigationReport(
			@PathVariable(value = "providerId") int providerId, @PathVariable(value = "mobile") String mobile) {

		List<UserInvestigationReport> userInvestigationReports = investigationRequestService
				.UserInvestigationReport(providerId, mobile);

		return ResponseEntity.ok().body(userInvestigationReports);
	}

	// http://localhost:8080/test
	@GetMapping("/test")
	public ResponseEntity<?> testInvestigation() throws ParseException {

		return ResponseEntity.ok().body("successfully updated");

	}


	// http://localhost:8080/pdfreport/1/159
	@GetMapping(value = "/pdfreport/{providerId}/{investigationRequestId}")
	public ResponseEntity<InputStreamResource> reportPdf(@PathVariable(value = "providerId") int providerId,
			@PathVariable(value = "investigationRequestId") int investigationRequestId) throws IOException {

		byte[] data = investigationRequestService.userReportPdf(investigationRequestId, providerId);
		ByteArrayInputStream bis = null;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Report.pdf");

		if (data != null) {
			bis = new ByteArrayInputStream(data);
			System.out.println("pdf is call " + investigationRequestId);
			return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_OCTET_STREAM)
					.body(new InputStreamResource(bis));
		}
		else {
			return ResponseEntity.badRequest().body(null);
		}


		// produces = MediaType.APPLICATION_PDF_VALUE
	}

//	http://localhost:8080/imagereportdownload/216
	@GetMapping(value = "/imagereportdownload/{investigationRequestId}")
	public ResponseEntity<InputStreamResource> imageReport(@PathVariable(value = "investigationRequestId") int investigationRequestId) {


		Optional<InvestigationRequest> in = investigationRequestRepository.findById(investigationRequestId);

		byte[] value = in.get().getReportImage();
		ByteArrayInputStream imagebyte = new ByteArrayInputStream(value);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=ReportImage.jpg");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_OCTET_STREAM)
				.body(new InputStreamResource(imagebyte));

		// ,
		// produces = MediaType.APPLICATION_PDF_VALUE, , produces = MediaType.IMAGE_JPEG_VALUE
	}

	// http://localhost:8080/investigationrequest/1
	@PostMapping("/investigationrequestn/{investigationrequestid}")
	ResponseEntity<?> updateInvestigation(@PathVariable(value = "investigationrequestid") int investigationRequestId,
			@RequestParam(required = false) MultipartFile reportImage,
			@RequestParam("investigationrequest") String investigationRequestObject)
			throws JsonParseException, JsonMappingException, IOException {

		InvestigationRequest investigationRequest = new ObjectMapper().readValue(investigationRequestObject,
				InvestigationRequest.class);

		investigationRequest.setImageFileName(reportImage.getOriginalFilename());
		investigationRequest.setReportImage(reportImage.getBytes());

		InvestigationRequest resultInvestigationRequest = investigationRequestService
				.updateInvestigationRequest(investigationRequestId, investigationRequest);

		if (resultInvestigationRequest != null) {
			return ResponseEntity.ok().body("successfully updated");
		} else {
			return ResponseEntity.badRequest().body("Fail to update");
		}
	}

	
}