package com.diagnostic.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@EntityListeners(AuditingEntityListener.class)
public class InvestigationRequest extends Auditable<String>{
	
	@Id
	@GeneratedValue
	int id;
	
	
	@NotBlank
	String status;
	
	String reportText;
	
    byte[] reportImage;
    
    String ImageFileName; 
    
//    @Transient
//    String reportImageEncode;
	
	int externalId;
	
	int referenceId;
	
	String reportUrl;
	
	String downloadStatus;
	
	@ManyToOne( optional = false)
	private ProviderInvestigation providerInvestigation;
	
	@ManyToOne( optional = false)
	private Investigation investigation;
	
	@ManyToOne( optional = false)
	private Patient patient;
	
	
	

	public int getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Investigation getInvestigation() {
		return investigation;
	}

	public void setInvestigation(Investigation investigation) {
		this.investigation = investigation;
	}

	public String getImageFileName() {
		return ImageFileName;
	}

	public void setImageFileName(String imageFileName) {
		ImageFileName = imageFileName;
	}


	public ProviderInvestigation getProviderInvestigation() {
		return providerInvestigation;
	}

	public void setProviderInvestigation(ProviderInvestigation providerInvestigation) {
		this.providerInvestigation = providerInvestigation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReportText() {
		return reportText;
	}

	public void setReportText(String reportText) {
		this.reportText = reportText;
	}

	public byte[] getReportImage() {
		return reportImage;
	}

	public void setReportImage(byte[] reportImage) {
		this.reportImage = reportImage;
	}

	public int getExternalId() {
		return externalId;
	}

	public void setExternalId(int externalId) {
		this.externalId = externalId;
	}

	public String getReportUrl() {
		return reportUrl;
	}

	public void setReportUrl(String reportUrl) {
		this.reportUrl = reportUrl;
	}

	public String getDownloadStatus() {
		return downloadStatus;
	}

	public void setDownloadStatus(String downloadStatus) {
		this.downloadStatus = downloadStatus;
	}
	
	
	
	

}
