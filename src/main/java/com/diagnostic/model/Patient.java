package com.diagnostic.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@EntityListeners(AuditingEntityListener.class)
public class Patient extends Auditable<String>{
	
	@Id
	@GeneratedValue
	int id;
	
	@NotBlank
	String name;
	
	@NotBlank
	String mobile; 
	
	@NotBlank
	String age;
	
	@NotBlank
	String sampleCollection;
	
	@NotBlank
	String appointmentDate;
	
	@NotBlank
	String nid;
	
	@NotBlank
	String gender;
	
	@NotBlank
	String email;
	
	@NotBlank
	String address;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getSampleCollection() {
		return sampleCollection;
	}

	public void setSampleCollection(String sampleCollection) {
		this.sampleCollection = sampleCollection;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", name=" + name + ", mobile=" + mobile + ", age=" + age + ", sampleCollection="
				+ sampleCollection + ", appointmentDate=" + appointmentDate + ", nid=" + nid + ", gender=" + gender
				+ ", email=" + email + ", address=" + address + "]";
	}
	
	
	
}
