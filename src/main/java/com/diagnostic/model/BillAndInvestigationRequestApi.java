package com.diagnostic.model;

public class BillAndInvestigationRequestApi {
	
	private Billing bill;
	
	private InvestigationRequest investigationRequest;

	public Billing getBill() {
		return bill;
	}

	public void setBill(Billing bill) {
		this.bill = bill;
	}

	public InvestigationRequest getInvestigationRequest() {
		return investigationRequest;
	}

	public void setInvestigationRequest(InvestigationRequest investigationRequest) {
		this.investigationRequest = investigationRequest;
	}
	
	

}
