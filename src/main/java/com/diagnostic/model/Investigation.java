package com.diagnostic.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;



@Table
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Investigation extends Auditable<String>{
	
	@Id
	@GeneratedValue
	int id;
	
	@NotBlank
	String name;	
	
	@NotBlank
	String description;
	
	@NotBlank
	String note;
	
	@NotBlank
	String format;
	
	@NotNull
	int rate;
	
	@NotNull
	String refValue;
	
	@ManyToOne( optional = false)
	private Category category;
	
	@ManyToOne( optional = false)
	private ProviderInvestigation providerInvestigation;
	
	@OneToMany(mappedBy = "investigation")
	private List<InvestigationRequest> investigationRequest;
	
//	@ManyToOne( optional = false)
//	private Test test;
	

	
	
	public ProviderInvestigation getProviderInvestigation() {
		return providerInvestigation;
	}
	public String getRefValue() {
		return refValue;
	}
	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}

	public void setProviderInvestigation(ProviderInvestigation providerInvestigation) {
		this.providerInvestigation = providerInvestigation;
	}
	
	
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		return "Investigation [id=" + id + ", name=" + name + ", description=" + description + ", note=" + note
				+ ", rate=" + rate + "]";
	}
	
	
	
}
