package com.diagnostic.model;

import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ProviderInvestigation {
	
	@Id
	@GeneratedValue
	int id;
	
	String medicalName;

	
	String note;
	
	
	@OneToMany(mappedBy = "providerInvestigation")
	private List<Investigation> investigation;
	
	

	public String getMedicalName() {
		return medicalName;
	}

	public void setMedicalName(String medicalName) {
		this.medicalName = medicalName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	
	
	

}
