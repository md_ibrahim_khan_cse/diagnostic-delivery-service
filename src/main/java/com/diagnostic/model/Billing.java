package com.diagnostic.model;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;


@Entity
public class Billing {
	
	@Id
	@GeneratedValue
	int id;
	
	@NotBlank
	int paymentValue;
	
	@NotBlank
	String paymentType;
	
    @OneToOne
    public Patient patient;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPaymentValue() {
		return paymentValue;
	}

	public void setPaymentValue(int paymentValue) {
		this.paymentValue = paymentValue;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	


	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	@Override
	public String toString() {
		return "Billing [id=" + id + ", paymentValue=" + paymentValue + ", paymentType=" + paymentType + "]";
	}
    
	
    
}
