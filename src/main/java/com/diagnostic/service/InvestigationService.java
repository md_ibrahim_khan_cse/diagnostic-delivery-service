package com.diagnostic.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diagnostic.model.Category;
import com.diagnostic.model.FormatResponce;
import com.diagnostic.model.Investigation;
import com.diagnostic.model.ProviderInvestigation;
import com.diagnostic.repository.CategoryRepository;
import com.diagnostic.repository.InvestigationRepository;
import com.diagnostic.repository.InvestigationRequestRepository;
import com.diagnostic.repository.ProviderInvestigationRepository;

@Service
public class InvestigationService {

	@Autowired
	private InvestigationRepository investigationRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ProviderInvestigationRepository providerInvestigationRepository;
	
	@Autowired
	private InvestigationRequestRepository investigationRequestRepository;

	public Investigation createInvestigation(int categoryId, Investigation requestInvestigation) {

		Optional<Category> categoryOptional = categoryRepository.findById(categoryId);

		if (categoryOptional.isPresent()) {
			// requestInvestigation.setCategory(categoryOptional.get());
			return investigationRepository.save(requestInvestigation);
		}

		return null;

	}

	// @SuppressWarnings("unused")
	public Investigation createInvestigationProviderInvestigationId(int providerInvestigationId, int categoryId,
			Investigation requestInvestigation) {

		Optional<ProviderInvestigation> providerInvestigation = providerInvestigationRepository
				.findById(providerInvestigationId);

		Optional<Category> categoryOptional = categoryRepository.findById(categoryId);

//		Test t = new Test();
//		t.setId(2);

		if (providerInvestigation.isPresent() && categoryOptional.isPresent()) {

			requestInvestigation.setCategory(categoryOptional.get());
			requestInvestigation.setProviderInvestigation(providerInvestigation.get());
			// requestInvestigation.setTest(t);
			return investigationRepository.save(requestInvestigation);
		}

		return null; // drop table investigation
	}

	public List<Investigation> fetchAllInvestigation() {

		return investigationRepository.findAll();

	}

	public List<Investigation> fetchInvestigationByCategoryId(int categoryId) {

		return null;

		// return investigationRepository.findByCategoryId(categoryId);
	}

	public Investigation updateInvestigation(int investigationId, int providerId, int categoryId, Investigation updateInvestigation) {


		Investigation investigation = investigationRepository.findByIdAndProviderInvestigationId(investigationId, providerId);
		Optional<Category> category =  categoryRepository.findById(categoryId);
		
		if (investigation != null && category.isPresent()) {

			investigation.setDescription(updateInvestigation.getDescription());
			investigation.setName(updateInvestigation.getName());
			investigation.setNote(updateInvestigation.getNote());
			investigation.setRate(updateInvestigation.getRate());
			investigation.setFormat(updateInvestigation.getFormat());
			investigation.setRefValue(updateInvestigation.getRefValue());
			investigation.setCategory(category.get());
			

			return investigationRepository.save(investigation);

		}

		return null;
	}

	public int deleteInvestigation(int providerId, int investigationId) {
		
		boolean delete = !investigationRequestRepository.existsByProviderInvestigationIdAndInvestigationId(providerId, investigationId) && 
				investigationRepository.existsByidAndProviderInvestigationId(investigationId, providerId);

		if (delete) {

			investigationRepository.deleteById(investigationId);
			return 1;

		}

		return 0;
	}


	public List<Investigation> fetchInvestigationByProviderId(int providerId) {
		
		return investigationRepository.findByProviderInvestigationId(providerId);
	}

	public List<Investigation> fetchInvestigationBySelectedItem(String[] selectedItem) {
		
		List<Investigation> investigationList = new ArrayList<>();
		
		for(int i=0; i<selectedItem.length; i++) {
			
			Optional<Investigation> investigation = investigationRepository.findById(Integer.parseInt(selectedItem[i]));
			if(investigation.isPresent()) {
				investigationList.add(investigation.get());
			}
		}
		
		return investigationList;
	}
	
	
	public List<String> investigationNameList(int providerId) {
		
		
		List<Investigation> list = investigationRepository.findByProviderInvestigationId(providerId);
		List<String> resultInvestigationRequestNameList = new ArrayList<>();
		
		//List<Investigation> list1 = investigationRepository.g;
		
		for(int i=0; i<list.size(); i++) {
			resultInvestigationRequestNameList.add(list.get(i).getName());
		}
		
		return resultInvestigationRequestNameList;
	}

	public FormatResponce fetchInvestigationFormatByProviderIdAndInvestigationName(int providerId,
			String investigationName) {
		FormatResponce formatResponce = 
				investigationRepository.getFormat(providerId, investigationName);
		
		return formatResponce;
	}

}
