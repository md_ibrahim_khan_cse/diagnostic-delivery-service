package com.diagnostic.service;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
 
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;




import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.stereotype.Service;

import com.diagnostic.model.AdminInvestigationPost;
import com.diagnostic.model.Billing;
import com.diagnostic.model.Investigation;
import com.diagnostic.model.InvestigationRequest;
import com.diagnostic.model.Patient;
import com.diagnostic.model.ProviderInvestigation;
import com.diagnostic.model.UserInvestigationReport;
import com.diagnostic.repository.BillingRepository;
import com.diagnostic.repository.InvestigationRepository;
import com.diagnostic.repository.InvestigationRequestRepository;
import com.diagnostic.repository.PatientRepository;
import com.diagnostic.repository.ProviderInvestigationRepository;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
//import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Service
public class InvestigationRequestService {

	@Autowired
	private InvestigationRequestRepository investigationRequestRepository;

	@Autowired
	private ProviderInvestigationRepository providerInvestigationRepository;

	@Autowired
	private InvestigationRepository investigationRepository;

	@Autowired
	private PatientRepository patientRepository;

	@Autowired
	private BillingService billingService;
	
	@Autowired
	private BillingRepository billingRepository;

	public List<InvestigationRequest> createInvestigationRequest(int providerId, String[] selectedItem,
			Patient patient) {

		Optional<ProviderInvestigation> providerInvestigation = providerInvestigationRepository.findById(providerId);
		List<InvestigationRequest> resultInvestigationRequest = new ArrayList<>();

		if (providerInvestigation.isPresent()) {

			System.out.println("patietnvaluebefore " + patient.toString());
			Patient patientResult = patientRepository.save(patient);
			System.out.println("patietnvalue " + patientResult.toString());

			for (int i = 0; i < selectedItem.length; i++) {

				Optional<Investigation> investigation = investigationRepository
						.findById(Integer.parseInt(selectedItem[i]));
				InvestigationRequest investigationRequest = new InvestigationRequest();

				if (investigation.isPresent()) {
					investigationRequest.setInvestigation(investigation.get());
					investigationRequest.setProviderInvestigation(providerInvestigation.get());
					investigationRequest.setPatient(patientResult);
					resultInvestigationRequest.add(investigationRequestRepository.save(investigationRequest));
				}

			}

			return resultInvestigationRequest;
		}

		return null;

	}

	public List<InvestigationRequest> fetchInvestigationRequestByProviderId(int providerId) {

		String image_encode;

		Optional<ProviderInvestigation> providerInvestigation = providerInvestigationRepository.findById(providerId);

		if (providerInvestigation.isPresent()) {

			List<InvestigationRequest> investigationRequestList = investigationRequestRepository
					.findByProviderInvestigationId(providerId);

			for (int i = 0; i < investigationRequestList.size(); i++) {

				if (investigationRequestList.get(i).getImageFileName() != null) {

					image_encode = convertToBase64(investigationRequestList.get(i).getImageFileName(),
							investigationRequestList.get(i).getReportImage());
					// investigationRequestList.get(i).setImage_encode(image_encode);
				}
			}

			System.out.println("Printing from  service " + investigationRequestList.size());
			return investigationRequestList;

		}

		return null;

	}

	public InvestigationRequest updateInvestigationRequest(int investigationRequestId,
			InvestigationRequest investigationRequest) {

		Optional<InvestigationRequest> optianalInvestigationRequest = investigationRequestRepository
				.findById(investigationRequestId);

		if (optianalInvestigationRequest.isPresent()) {
			optianalInvestigationRequest.get().setImageFileName(investigationRequest.getImageFileName());
			optianalInvestigationRequest.get().setStatus(investigationRequest.getStatus());
			optianalInvestigationRequest.get().setReportImage(investigationRequest.getReportImage());
			optianalInvestigationRequest.get().setReportText(investigationRequest.getReportText());

			return investigationRequestRepository.save(optianalInvestigationRequest.get());
		}

		return null;
	}

	public byte[] userReportPdf(int investigationRequestId, int providerId) throws IOException {

		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		UserInvestigationReport investigationReport = investigationRequestRepository
				.getUserReportPDf(investigationRequestId, providerId);
		int count = 1, y_axis = 270;


		if (investigationReport == null) {
			System.out.println("providerId null = " + providerId + " " + investigationRequestId + " ");
		} else {
			System.out.println("providerId not null = " + providerId + " " + investigationRequestId + " "
					+ investigationReport.toString());
		}
		if (investigationReport != null) {

			try {

				PdfWriter.getInstance(document, out);
				document.open();

				Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 25.0f, Font.UNDERLINE | Font.BOLD,
						BaseColor.BLACK);
				Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 13.0f, Font.BOLD, BaseColor.BLACK);
				Font commonFont = new Font(FontFamily.TIMES_ROMAN, 12.0f);
				Chunk firstPart = new Chunk("Name ", boldFont);
				Chunk secondPart = new Chunk("", commonFont);

				Paragraph heading = new Paragraph("Medical Report", headerFont);

				PdfPTable table = new PdfPTable(new float[] { 25, 8, 62.5f });
				table.setWidthPercentage(90);
				table.setHorizontalAlignment(Element.ALIGN_LEFT);
				table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.getDefaultCell().setFixedHeight(35);

				table.addCell(new Phrase(firstPart));
				table.addCell(new Phrase(new Chunk(":", boldFont)));
				table.addCell(new Phrase(new Chunk(investigationReport.getPatientName(), commonFont)));
				


				if (investigationReport.getAge() != null && !investigationReport.getAge().isEmpty()) {
					table.addCell(new Phrase(new Chunk("Age ", boldFont)));
					table.addCell(new Phrase(new Chunk(":", boldFont)));
					table.addCell(new Phrase(new Chunk(investigationReport.getAge(), commonFont)));
					count++;
				}

				if (investigationReport.getGender() != null && !investigationReport.getGender().isEmpty()) {
					table.addCell(new Phrase(new Chunk("Sex ", boldFont)));
					table.addCell(new Phrase(new Chunk(":", boldFont)));
					table.addCell(new Phrase(new Chunk(investigationReport.getGender(), commonFont)));
					count++;
				}

				if (investigationReport.getInvestigationName() != null && !investigationReport.getInvestigationName().isEmpty()) {
					table.addCell(new Phrase(new Chunk("Test Name ", boldFont)));
					table.addCell(new Phrase(new Chunk(":", boldFont)));
					table.addCell(new Phrase(new Chunk(investigationReport.getInvestigationName(), commonFont)));

					table.addCell(new Phrase(new Chunk("Cost                 ", boldFont)));
					table.addCell(new Phrase(new Chunk(":", boldFont)));
					table.addCell(new Phrase(new Chunk(Integer.toString(investigationReport.getRate()), commonFont)));
					count+=2;
				}

				if (investigationReport.getReportDate() != null && !investigationReport.getReportDate().toString().isEmpty()) {

					table.addCell(new Phrase(new Chunk("Report Create Date     ", boldFont)));
					table.addCell(new Phrase(new Chunk(":", boldFont)));
					table.addCell(new Phrase(new Chunk(investigationReport.getReportDate(), commonFont)));
					count++;
				}

				if (investigationReport.isReportText()) {
					table.addCell(new Phrase(new Chunk("Report Result", boldFont)));
					table.addCell(new Phrase(new Chunk(":", boldFont)));
					table.addCell(new Phrase(new Chunk(investigationReport.getReportTextValue(), commonFont)));
					count++;
				}

				if (investigationReport.isReportImage()) {

					table.addCell(new Phrase(new Chunk("Report Image ", boldFont)));
					table.addCell(new Phrase(new Chunk(":", boldFont)));
					table.addCell("");
					
				
					
				
					ByteArrayInputStream imagebyte = new ByteArrayInputStream(investigationReport.getReportImageValue());
					Image reportImage = Image.getInstance(investigationReport.getReportImageValue());
					reportImage.scaleAbsolute(200, 200);
					reportImage.setAbsolutePosition(200, y_axis + (7-count)*30);
					document.add(reportImage);
				}

				heading.setAlignment(Element.ALIGN_CENTER);
				heading.setSpacingAfter(40f);

				document.add(heading);
				document.add(table);
				document.close();
//			document.add(patienName);
//			document.add(investigationName);
//			document.add(rate);
//			document.add(createdDate);
//			document.add(reportText);

//			Style normal = new Style();
//			PdfFont font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
//			normal.setFont(font).setFontSize(14);
//			
//			Style code = new Style();
//			PdfFont monospace = PdfFontFactory.createFont(FontConstants.COURIER);
//			code.setFont(monospace).setFontColor(Color.RED)
//			    .setBackgroundColor(Color.LIGHT_GRAY);

			} catch (DocumentException ex) {

				System.out.print("Error occurred: {0}" + ex);
			}

			return out.toByteArray();
		} else {
			System.out.println("byte null ");
		}

		// return new ByteArrayInputStream(out.toByteArray());
		return null;

	}

	public String convertToBase64(String fileName, byte[] logo) {

		String extension = FilenameUtils.getExtension(fileName);
		String encodeBase64 = Base64.getEncoder().encodeToString(logo);

		return "data:image/" + extension + ";base64," + encodeBase64;

	}

	public InvestigationRequest createInvestigationRequestByAdmin(int providerId, AdminInvestigationPost object) {

		Optional<ProviderInvestigation> providerInvestigation = providerInvestigationRepository.findById(providerId);
		Patient patient = new Patient();
		InvestigationRequest investigationRequest = new InvestigationRequest();
		Billing bill = new Billing();

		if (providerInvestigation.isPresent()) {

			patient.setName(object.getpatientName());
			patient.setMobile(object.getMobile());
			patient.setSampleCollection("From Diagnosis");
			patient.setAge(object.getAge());
			patient.setGender(object.getGender());
			patient.setEmail(object.getGmail());
			Patient resultedPatient = patientRepository.save(patient);

			Optional<Investigation> resultedInvestigation = investigationRepository
					.findByNameAndProviderInvestigationId(object.getInvestigationName(), providerId);

//			Optional<Investigation> resultedInvestigation = null;

			if (resultedInvestigation.isPresent() && resultedPatient != null) {

				if (object.isBillStatus()) {
					billingService.createBill(resultedPatient.getId(), bill);
				}

				investigationRequest.setImageFileName(object.getImageFileName());
				investigationRequest.setReportImage(object.getReportImage());
				investigationRequest.setReferenceId(object.getReferenceId());
				investigationRequest.setReportText(object.getReportText());
				investigationRequest.setStatus("Complete");
				investigationRequest.setInvestigation(resultedInvestigation.get());
				investigationRequest.setPatient(resultedPatient);
				investigationRequest.setProviderInvestigation(providerInvestigation.get());
				return investigationRequestRepository.save(investigationRequest);

			}
		}
		return null;
	}

	public List<UserInvestigationReport> UserInvestigationReport(int providerId, String mobile) {

		List<UserInvestigationReport> userInvestigationReports = investigationRequestRepository
				.getUserReportList(providerId, mobile);
		int length = userInvestigationReports.size();
		for (int i = 0; i < length; i++) {
			userInvestigationReports.get(i).setReportImageValue(null);
			userInvestigationReports.get(i).setBillStatus(billingRepository.existsByPatientId(userInvestigationReports.get(i).getPatientId()));
		}

		return userInvestigationReports;
	}

	private JavaMailSender javaMailSender;

	@Autowired
	public void MailService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public int sendMailToPatient(String gmail, int providerId, int investigationRequestId)
			throws MailException, MessagingException, IOException {

		//System.out.println("gmail " + gmail + " " + providerId);

		Optional<ProviderInvestigation> providerInvestigation = providerInvestigationRepository
				.findById(investigationRequestId);
		//System.out.println("providerInvestigation naem outside=" + providerInvestigation.get().getMedicalName());
		byte[] data = userReportPdf(providerId, investigationRequestId);
		//data = null;
		
		
		if (providerInvestigation.isPresent() && data != null) {

			//System.out.println("providerInvestigation naem " + providerInvestigation.get().getMedicalName());

			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

			helper.setFrom("gmailAddress");
			helper.setTo(gmail);
			helper.setSubject("Medical Report From " + providerInvestigation.get().getMedicalName());
			helper.setText("Please download the below file.");

//		ClassPathResource classPathResource = new ClassPathResource("Attachment.pdf");
//		ByteArrayInputStream byteArrayFileObj = userReportPdf(1,1);

			final InputStreamSource fileStreamSource = new ByteArrayResource(data);
			helper.addAttachment("Report.pdf", fileStreamSource);

			javaMailSender.send(mimeMessage);
			return 1;
		}

//		SimpleMailMessage mail = new SimpleMailMessage();
//		mail.setTo(gmail);
//		mail.setSubject("Testing Mail API");
//		mail.setText("Hurray ! You have done that dude...");
//		javaMailSender.send(mail);

		return 0;
	}

}

//table.addCell(new Phrase(new Chunk("cell 7")));
//table.addCell("Text 7 ");
//table.addCell(new Phrase(new Chunk("cell")));

// ImageData data =
// ImageDataFactory.create(investigationReport.getReportImageValue());
//
//patienName.setSpacingAfter(8f);
//investigationName.setSpacingAfter(8f);
//rate.setSpacingAfter(8f);
//createdDate.setSpacingAfter(8f);
//reportText.setSpacingAfter(8f);
//Paragraph patienName = new Paragraph();
//secondPart = new Chunk(investigationReport.getPatientName(), commonFont);
//patienName.add(firstPart);
//patienName.add(secondPart);
//
//Paragraph investigationName = new Paragraph();
//firstPart = new Chunk("Service Name : ", boldFont);
//secondPart = new Chunk(investigationReport.getInvestigationName(), commonFont);
//investigationName.add(firstPart);
//investigationName.add(secondPart);
//
//Paragraph rate = new Paragraph();
//firstPart = new Chunk("Cost: ", boldFont);
//secondPart = new Chunk(Integer.toString(investigationReport.getRate()), commonFont);
//rate.add(firstPart);
//rate.add(secondPart);
//
//Paragraph createdDate = new Paragraph();
//firstPart = new Chunk("Report Create Date : ", boldFont);
//secondPart = new Chunk(investigationReport.getCreated_date(), commonFont);
//createdDate.add(firstPart);
//createdDate.add(secondPart);
//
//Paragraph reportText = new Paragraph();
//firstPart = new Chunk("Report : ", boldFont);
//secondPart = new Chunk(investigationReport.getReportTextValue(), commonFont);
//reportText.add(firstPart);
//reportText.add(secondPart);
//

//Date date = new Date();
//SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm");
//
//System.out.println("timeFormat " + timeFormat.format(date).toString());
//
//investigationRequest.setDate(dateFormat.format(date).toString());
//investigationRequest.setHour(timeFormat.format(date).toString().substring(0, 2));
//investigationRequest.setMinute(timeFormat.format(date).toString().substring(3));
//investigationRequest.setStatus("Pandding");
//investigationRequest.setReportUrl("Not Implement Yet");
//investigationRequest.setDownloadStatus("Not Implement Yet");



//{"patientName":"Md Ibrahim Khan","mobile":"01521453788","investigationName":"Calcium blood test","reportText":"Corona nagative","referenceId":"2","billStatus":"true","gender":"Male","age":"24"}
