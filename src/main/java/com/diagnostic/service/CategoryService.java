package com.diagnostic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diagnostic.model.Category;
import com.diagnostic.repository.CategoryRepository;



@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	public int CreateCategory(Category category) {
		
		if(categoryRepository.save(category) != null) {
			return 1;
		}
		else {
			return 0;
		}
		
	}

	public Category updateCategory(int categoryId, Category updateCategory) {
		
		
		Optional<Category> category = categoryRepository.findById(categoryId);
		
		if(category.isPresent()) {
			
			category.get().setName(updateCategory.getName());
			category.get().setDescription(updateCategory.getDescription());
			
			return categoryRepository.save(category.get());
			
		}
		
		return null;
		
	}

	public List<Category> fetchAllCategory() {
		
		return categoryRepository.findAll();
		
	}
	
}
