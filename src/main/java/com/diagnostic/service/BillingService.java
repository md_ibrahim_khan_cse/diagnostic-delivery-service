package com.diagnostic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diagnostic.model.Billing;
import com.diagnostic.model.InvestigationRequest;
import com.diagnostic.model.Patient;
import com.diagnostic.repository.BillingRepository;
import com.diagnostic.repository.InvestigationRequestRepository;
import com.diagnostic.repository.PatientRepository;

@Service
public class BillingService {

	
	@Autowired
	private BillingRepository billingRepository;
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private InvestigationRequestRepository investigationRequestRepository;
	
	public Billing createBill(int patientId, Billing bill) {
		
		Optional<Patient> optionalPatient = 
				patientRepository.findById(patientId);
		
		if(optionalPatient.isPresent()) {
			
			System.out.println("inside loop.... " + patientId);
			bill.setPatient(optionalPatient.get());
			return billingRepository.save(bill);
			
		}
		
		return null;
		
	}
	
	public List<Billing> fetchAllBill() {
		
		List<Billing> billingList = billingRepository.findAll();
		return billingList;
	}
	
	
	public Billing fetchBillingbyinvestigationRequestId(int investigationRequestId) {
		
		//return billingRepository.findByInvestigationRequestId(investigationRequestId);
		return null;
		
	}
	
	public boolean isBillingPresentByThisPatientId(int patientId) {
		Optional<Billing> bill = billingRepository.findByPatientId(patientId);
		
		if(bill.isPresent()) {
			return true;
		}
		
		return false;
		
	}
	
}
