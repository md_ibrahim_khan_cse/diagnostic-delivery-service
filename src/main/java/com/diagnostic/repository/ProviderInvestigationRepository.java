package com.diagnostic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.diagnostic.model.ProviderInvestigation;

public interface ProviderInvestigationRepository extends JpaRepository<ProviderInvestigation, Integer>{

}
