package com.diagnostic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.diagnostic.model.FormatResponce;
import com.diagnostic.model.Investigation;
import com.diagnostic.model.UserInvestigationReport;

public interface InvestigationRepository extends JpaRepository<Investigation, Integer>{

	List<Investigation> findByCategoryId(int categoryId);
	
	List<Investigation> findByProviderInvestigationId(int providerId);
	
	boolean existsByid(int investigationId);
	
	@Query("select i.name from Investigation i where  i.providerInvestigation.id = ?1" )
    List<String> getNameAndRateOnly(int providerId);
	
	@Query("select i.format from Investigation i where  i.providerInvestigation.id = ?1 and i.name = ?2" )
    String fetchInvestigationNameByProviderIdAndInvestigationName(int providerId, String investigationName);
	
	Optional<Investigation> findByNameAndProviderInvestigationId(String Name, int providerInvestigationId);
	
	
	@Query("SELECT new com.diagnostic.model.FormatResponce(i.format, i.refValue, i.category.name) "
			+ "FROM Investigation i  where i.providerInvestigation.id = ?1 and i.name =?2")
	FormatResponce getFormat(int providerId, String investigationName);

	boolean existsByidAndProviderInvestigationId(int investigationId, int providerId);

	Investigation findByIdAndProviderInvestigationId(int investigationId, int providerId);
	
	
//	@Query("select i.name, i.rate from Investigation i" )
//	interface ProjectIdAndName{
//	    String getId();
//	    String getName();
//	}
//	List<ProjectIdAndName> findAll();
	
}
