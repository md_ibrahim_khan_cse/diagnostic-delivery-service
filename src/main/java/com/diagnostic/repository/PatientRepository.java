package com.diagnostic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.diagnostic.model.InvestigationRequest;
import com.diagnostic.model.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer>{

}
