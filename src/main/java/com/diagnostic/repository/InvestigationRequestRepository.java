package com.diagnostic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diagnostic.model.AdminInvestigationPost;
import com.diagnostic.model.InvestigationRequest;
import com.diagnostic.model.UserInvestigationReport;

public interface InvestigationRequestRepository extends JpaRepository<InvestigationRequest, Integer>{

	
	List<InvestigationRequest> findByProviderInvestigationId(int providerId);
	
	@Query("select IR.reportText from InvestigationRequest IR  where  IR.patient.mobile = ?1" )
    List<String> getUserReport(String mobile);
	
	
	@Query("SELECT new com.diagnostic.model.UserInvestigationReport(i.id, p.id, p.name, p.age, p.gender, i.investigation.name, i.investigation.category.name, i.investigation.rate, i.update_date, i.referenceId, i.reportImage, i.reportText) "
			+ "FROM InvestigationRequest i JOIN i.patient p where i.providerInvestigation.id = ?1 and p.mobile =?2")
	List<UserInvestigationReport> getUserReportList(int providerId, String mobile);
	
	@Query("SELECT new com.diagnostic.model.UserInvestigationReport(i.id, p.id, p.name, p.age, p.gender, i.investigation.name, i.investigation.category.name, i.investigation.rate, i.update_date, i.referenceId, i.reportImage, i.reportText) "
			+ "FROM InvestigationRequest i JOIN i.patient p where  i.id =:investigationRequestId and i.providerInvestigation.id =:providerId")
	UserInvestigationReport getUserReportPDf(@Param("investigationRequestId") int investigationRequestId, @Param("providerId") int providerId);

	boolean existsByProviderInvestigationIdAndInvestigationId(int providerId, int investigationId);
	
}
