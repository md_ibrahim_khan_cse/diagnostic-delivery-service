package com.diagnostic.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.diagnostic.model.Billing;

public interface BillingRepository extends JpaRepository<Billing, Integer>{

  Optional<Billing> findByPatientId(int patientId);

  boolean existsByPatientId(int patientId);

  

	
}
