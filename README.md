Spring Boot Project Configaration:

1. Create a database with the name of diagnostic-delivery-service
2. Put your email id and password in application.properties file and also put your email id in InvestigationRequestService class at 415 number line.

Note: Make sure less secure App is enabled for your E-mail id that you are going to use.

3. After run the project you need to execute the below sql statement.

INSERT INTO `provider_investigation` (`id`, `note`, `medical_name`) VALUES
(1, '', 'Khulna Medical Hospital'),
(2, '', 'Shaheed Sheikh Abu Naser Specialised Hospital'),
(3, NULL, 'Khulna City Hospital'),
(4, NULL, 'Islami Bank Hospital Khulna'),
(5, NULL, 'Khulna Shishu Hospital'),
(6, NULL, 'Gazi Medical College and Hospital');

INSERT INTO `category` (`id`, `created_date`, `update_date`, `description`, `name`) VALUES
(13, '2008-09-20 00:00:00', '2008-09-20 00:00:00', '', 'Pathology'),
(14, '2008-09-20 00:00:00', '2008-09-20 00:00:00', '', 'Cardiology');

